﻿using CadastroAcoesBolsaValores.model;
using System;
using System.Collections.Generic;

namespace CadastroAcoesBolsaValores
{
    class Carteira
    {
        public static List<Acao> acoes = new List<Acao>();

        public void cadastrarAcao(String codigo, int qtd)
        {
            foreach (var Acao in acoes)
            {
                if (Acao.codigo == codigo)
                {
                    Acao.quantidade += qtd;
                }
                else
                {
                    Acao acao = new Acao(codigo, qtd);
                    acoes.Add(acao);
                }
            }
        }

        public void pesquisarAcao(String codigo)
        {
            foreach (var acao in acoes)
            {
                if (acao.codigo == codigo)
                {
                    Console.WriteLine(acao.codigo + " - " + acao.quantidade);
                } else
                {
                    Console.WriteLine("Não Obteve Retorno");
                }

                
            }
        }

        public void visualizarCarteiraAcoes()
        {
            int indexador = 0;
            foreach (var acao in acoes)
            {
                indexador += 1;
                Console.WriteLine(indexador + ": " + acao.codigo + " - " + acao.quantidade);
            }
        }

        
    }
}
