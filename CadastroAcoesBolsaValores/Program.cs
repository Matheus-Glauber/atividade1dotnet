﻿using CadastroAcoesBolsaValores.model;
using System;
using System.Collections.Generic;

namespace CadastroAcoesBolsaValores
{
    class Program
    {
        static void Main(string[] args)
        {
            Acao acao = new Acao();
            Carteira listaDeAcoes = new Carteira();
            List<Acao> listaAcoes = new List<Acao>();

            Acao acao1 = new Acao("MGLU", 2);
            Acao acao2 = new Acao("MOND", 4);
            Acao acao3 = new Acao("ELPS", 2);

            listaAcoes.Add(acao1);
            listaAcoes.Add(acao2);
            listaAcoes.Add(acao3);

            foreach (var Acao in listaAcoes)
            {
                Console.WriteLine(Acao.codigo + " - " + Acao.quantidade);
            }

            Boolean continuar = true;
            
            while (continuar)
            {
                String opcao;
                Console.WriteLine("Opções:");
                Console.WriteLine("1 - Cadastro de ação,");
                Console.WriteLine("2 - Pesquisar por ação,");
                Console.WriteLine("3 - Visualizar a carteira de ações...");
                opcao = Console.ReadLine();
                Console.WriteLine("");

                if(opcao == "1")
                {
                    string codigo;
                    string quantidadeString;
                    int quantidade;

                    Console.WriteLine("Código da Ação: ");
                    codigo = Console.ReadLine();
                    Console.WriteLine("Quantidade da ação: ");
                    quantidadeString = Console.ReadLine();
                    quantidade = Convert.ToInt32(quantidadeString);

                    listaDeAcoes.cadastrarAcao(codigo, quantidade);
                }

                else if(opcao == "2")
                {
                    string codigo;

                    Console.WriteLine("Pesquisar pelo nome do código: ");
                    codigo = Console.ReadLine();

                    listaDeAcoes.pesquisarAcao(codigo);
                }

                else if(opcao == "3")
                {
                    listaDeAcoes.visualizarCarteiraAcoes();
                }

                Console.WriteLine("Continuar? [S - N]");
                opcao = Console.ReadLine();
                if(opcao == "S")
                {
                    continuar = true;
                }
                else
                {
                    continuar = false;
                }
            }
            
        }
    }
}
