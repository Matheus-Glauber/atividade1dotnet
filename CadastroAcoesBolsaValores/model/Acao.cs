﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CadastroAcoesBolsaValores.model
{
    class Acao
    {
        public String codigo { get; set; }
        public int quantidade { get; set; }

        public Acao(string codigo, int quantidade)
        {
            this.codigo = codigo;
            this.quantidade = quantidade;
        }

        public Acao()
        { }

    }
}
